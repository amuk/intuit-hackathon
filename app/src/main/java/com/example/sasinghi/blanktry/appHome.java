package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by sasinghi on 3/25/2015.
 */
public class appHome extends Activity {

    private Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
       /* register = (Button) findViewById(R.id.registerButton);
        register.setOnClickListener(this);*/

    }

    public void onRegisterClick(View v){
        Intent i = new Intent(appHome.this, registration.class);
        startActivity(i);
    }

    public void onLoginClick(View v){
        new MaterialDialog.Builder(this)
                .customView(R.layout.login_dialog, true)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .negativeColor(R.color.accent_material_light)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        EditText userName = (EditText) dialog.findViewById(R.id.user);
                        EditText password = (EditText) dialog.findViewById(R.id.pass);
                        if (userName != null && password != null) {
                            //Toast.makeText(getApplicationContext(), "User:" + userName.getText().toString() + " Pass:" + password.getText().toString(), Toast.LENGTH_SHORT).show();
                            // Check credentials. Return if user is biz or customer. False if wrong credentials.
                            String checkUser = userName.getText().toString();
                            String checkPass = password.getText().toString();
                            if (checkUser.equalsIgnoreCase("customer") && checkPass.equalsIgnoreCase("customer")) {
                                Intent i = new Intent(appHome.this, customerHome.class);
                                startActivity(i);
                            } else if (checkUser.equalsIgnoreCase("business") && checkPass.equalsIgnoreCase("business")) {
                                Intent i = new Intent(appHome.this, businessHome.class);
                                startActivity(i);
                            } else {
                                Toast.makeText(getApplicationContext(), "Incorrect credentials. Try again.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                })
                .show();
    }


}
