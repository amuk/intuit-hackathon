package com.example.sasinghi.blanktry;

/**
 * Created by sasinghi on 3/29/2015.
 */
public class searchResultPOJO {
    private String businessName;
    private String businessContact;
    private String businessDistance;


    public searchResultPOJO(String businessName, String businessContact, String businessDistance) {
        this.businessName = businessName;
        this.businessContact = businessContact;
        this.businessDistance = businessDistance;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessContact() {
        return businessContact;
    }

    public void setBusinessContact(String businessContact) {
        this.businessContact = businessContact;
    }

    public String getBusinessDistance() {
        return businessDistance;
    }

    public void setBusinessDistance(String businessDistance) {
        this.businessDistance = businessDistance;
    }
}
