package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * Created by sasinghi on 3/28/2015.
 */
public class businessHome extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.business_home);

    }

    public void onViewSalesClicked(View v){
        Intent i = new Intent(businessHome.this, showAnalysis.class);
        startActivity(i);
    }

    public void onNewBillClicked(View v){
        Intent i = new Intent(businessHome.this, createBill.class);
        startActivity(i);
    }
}