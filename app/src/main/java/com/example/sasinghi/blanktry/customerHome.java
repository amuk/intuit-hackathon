package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by sasinghi on 3/28/2015.
 */
public class customerHome extends Activity {
    String service_category,search_radius;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_home);
        final String[] categories= {"Tailoring","Painting","Plumbing","Carpentry","Hardware","Tutoring","Household"};
        final Spinner category = (Spinner) findViewById(R.id.categories);
        ArrayAdapter<String> adapter_category = new ArrayAdapter<String>(this,  android.R.layout.simple_spinner_item, categories);
        category.setAdapter(adapter_category);
        category.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
               @Override
               public void onItemSelected(AdapterView<?> arg0, View arg1,
                                          int arg2, long arg3) {
                   int position_category = category.getSelectedItemPosition();
                   service_category = categories[+position_category];


               }
               @Override
               public void onNothingSelected(AdapterView<?> arg0) {
                   // TODO Auto-generated method stub
               }
           }
        );

        final String[] proximity = {"5 kms","10 kms","15 kms"};
        final Spinner radius = (Spinner) findViewById(R.id.proximity);
        ArrayAdapter<String> adapter_radius = new ArrayAdapter<String>(this,  android.R.layout.simple_spinner_item, proximity);
        radius.setAdapter(adapter_radius);
        radius.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1,
                                               int arg2, long arg3) {
                        int position = radius.getSelectedItemPosition();
                        search_radius=proximity[+position];


                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                    }
                }
        );

    }

      public void onSearchClicked(View v){
          Toast.makeText(getApplicationContext(), "Search with service:"+service_category+"and radius:"+search_radius, Toast.LENGTH_SHORT).show();
          Intent i = new Intent(customerHome.this, searchResult.class);
          i.putExtra("proximity",search_radius);
          i.putExtra("category",service_category);
          startActivity(i);
      }

}
