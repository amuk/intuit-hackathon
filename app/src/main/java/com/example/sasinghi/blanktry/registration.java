package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by sasinghi on 3/26/2015.
 */
public class registration extends Activity {
    RadioGroup userType = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        String selection = null;
        userType = (RadioGroup) findViewById(R.id.radioGroup);
        final EditText nameHolder = (EditText) findViewById(R.id.name);
        final Button registerButton  = (Button) findViewById(R.id.registerButton);
        userType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.business:
                        nameHolder.setHint("Business Name");
                        registerButton.setText("Next");
                        break;
                    case R.id.customer:
                        nameHolder.setHint("Your Name");
                        break;

                }
            }
        });

    }
    public void  onRegisterClicked(View v){
        if(userType.getCheckedRadioButtonId()!=-1) {
            int id = userType.getCheckedRadioButtonId();
            View radioButton = userType.findViewById(id);
            int radioId = userType.indexOfChild(radioButton);
            RadioButton btn = (RadioButton) userType.getChildAt(radioId);
            String selection = (String) btn.getText();
            // reading all data
            ArrayList<String> collectedInfo = new ArrayList<String>();
            EditText name = (EditText) findViewById(R.id.name);
            EditText street = (EditText) findViewById(R.id.postalAddressStreet);
            EditText landmark = (EditText) findViewById(R.id.postalAddressLandMark);
            EditText country = (EditText) findViewById(R.id.country);
            EditText pinCode = (EditText) findViewById(R.id.pinCode);
            EditText userName = (EditText) findViewById(R.id.userName);
            EditText password = (EditText) findViewById(R.id.password);
            EditText rePassword = (EditText) findViewById(R.id.rePass);
            EditText email = (EditText) findViewById(R.id.email);
            EditText phone = (EditText) findViewById(R.id.phone);
            if (phone != null && name != null && street != null && landmark != null && country != null && pinCode != null && userName != null && password != null && rePassword != null) {
                if (name.getText().toString().isEmpty() || street.getText().toString().isEmpty() || country.getText().toString().isEmpty() || userName.getText().toString().isEmpty() || pinCode.getText().toString().isEmpty() || password.getText().toString().isEmpty() || rePassword.getText().toString().isEmpty() || email.getText().toString().isEmpty() || phone.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "All fields are mandatory.", Toast.LENGTH_SHORT).show();
                } else {
                    if (password.getText().toString().equalsIgnoreCase(rePassword.getText().toString())) {
                        if (selection.equalsIgnoreCase("Service Provider")) {
                            collectedInfo.add(name.getText().toString());
                            collectedInfo.add(street.getText().toString());
                            collectedInfo.add(landmark.getText().toString());
                            collectedInfo.add(country.getText().toString());
                            collectedInfo.add(email.getText().toString());
                            collectedInfo.add(pinCode.getText().toString());
                            collectedInfo.add(userName.getText().toString());
                            collectedInfo.add(password.getText().toString());
                            collectedInfo.add(rePassword.getText().toString());
                            collectedInfo.add(phone.getText().toString());
                            Intent i = new Intent(registration.this, bizRegistration.class);
                            i.putStringArrayListExtra("collectedInfo", collectedInfo);
                            startActivity(i);
                        } else {
                            //Store data foe customer.
                            Toast.makeText(getApplicationContext(), "Please login to continue.", Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(registration.this, appHome.class);
                            startActivity(i);
                        }
                    } else
                        Toast.makeText(getApplicationContext(), "Entered passwords do not match.", Toast.LENGTH_SHORT).show();

                }
                // data read. Now behaviour based on selection.
            }
        }
        else
            Toast.makeText(getApplicationContext(), "A user type needs to be selected.", Toast.LENGTH_SHORT).show();
    }
}
