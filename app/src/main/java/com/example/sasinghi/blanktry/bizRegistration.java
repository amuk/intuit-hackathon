package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by sasinghi on 3/28/2015.
 */
public class bizRegistration extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.biz_registration);

    }

    public void onBizRegisterClicked(View v){
        Intent i = getIntent();
        ArrayList<String> collectedInfo = i.getStringArrayListExtra("collectedInfo");
        String tags = "";
        CheckBox tagsChecked = (CheckBox) findViewById(R.id.tailor);
        if( tagsChecked.isChecked()){
            tags += "Tailor";
        }
        tagsChecked = (CheckBox) findViewById(R.id.painter);
        if( tagsChecked.isChecked()){
            tags += "Painter";
        }
        tagsChecked = (CheckBox) findViewById(R.id.plumber);
        if( tagsChecked.isChecked()){
            tags += "Plumber";
        }
        tagsChecked = (CheckBox) findViewById(R.id.carpenter);
        if( tagsChecked.isChecked()){
            tags += "Carpenter";
        }
        tagsChecked = (CheckBox) findViewById(R.id.maid);
        if( tagsChecked.isChecked()){
            tags += "Maid";
        }

        EditText otherTags = (EditText) findViewById(R.id.otherTags);
        if(!otherTags.getText().toString().isEmpty()){
            tags += otherTags.getText().toString();
        }

        // remove
        // Store collectedInfo and tags in biz DB
        Toast.makeText(getApplicationContext(), "Please Login to continue.", Toast.LENGTH_SHORT).show();
        // remove
        startActivity(new Intent(bizRegistration.this,appHome.class));
    }
}
