package com.cisco.ihack;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
 

import com.mongodb.DB;

@Path("/IHackService")
public class IHackSeviceImpl {
	
	IHackDAO ihackdao;
	
	// Test
	@GET
	@Path("/{param}")
	public Response getMsg(@PathParam("param") String msg) { 
		String output = "Jersey say : " + msg; 
		return Response.status(200).entity(output).build(); 
	}

	// Add a Service Provider
	@POST
	@Path("/addASerProv")
	@Produces("application/json")
	public String addSerProv(String jsonNewSP) {		
		IHackAction ihackaction = new IHackAction();
		DB IHackDatabase = ihackaction.initialize();
		IHackDAO ihackdao = new IHackDAO(IHackDatabase);	
		
		try {	
			return ihackdao.addServProviderDetails(jsonNewSP);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Service Provider could not be added";
	}
	
	
	//Add a customer
	@POST
	@Path("/addACust")
	@Produces("application/json")
	public String addCust(String jsonNewCust) {		
		IHackAction ihackaction = new IHackAction();
		DB IHackDatabase = ihackaction.initialize();
		IHackDAO ihackdao = new IHackDAO(IHackDatabase);	
		
		try {	
			return ihackdao.addCustomerDetails(jsonNewCust);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Customer could not be added";
	}
	
	
	//Add a new product to the SP
	@POST
	@Path("/addAProd")
	@Produces("application/json")
	public String addProd(String jsonNewProd) {		
		IHackAction ihackaction = new IHackAction();
		DB IHackDatabase = ihackaction.initialize();
		IHackDAO ihackdao = new IHackDAO(IHackDatabase);	
		
		try {	
			return ihackdao.addNewProduct(jsonNewProd);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Product could not be added";
	}
	
	
	//Update Current Loc of SP
	@GET
	@Path("/updateLoc")	
	public Response updateLoc(String jsonLoc) {		
		IHackAction ihackaction = new IHackAction();
		DB IHackDatabase = ihackaction.initialize();
		IHackDAO ihackdao = new IHackDAO(IHackDatabase);	
		
		ihackdao.updateCurrentLoc(jsonLoc);	
		return Response.status(200).entity("success").build(); 
	}
	
	
	//Get all products for an SP
	@GET
	@Path("/getAllProds")
	@Produces("application/json")
	public String getallprods(@QueryParam("id") String id){
		
		IHackAction ihackaction = new IHackAction();
		DB IHackDatabase = ihackaction.initialize();
		IHackDAO ihackdao = new IHackDAO(IHackDatabase);	
			
		try {
			return ihackdao.getAllProducts(id);			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "Could not retrieve products";
	}
	
	
	//Check the login credentials
	@POST
	@Path("/checkCred")
	@Produces("application/json")
	public String checkCreden(String jsonCred) {		
		IHackAction ihackaction = new IHackAction();
		DB IHackDatabase = ihackaction.initialize();
		IHackDAO ihackdao = new IHackDAO(IHackDatabase);	
		
		try {	
			return ihackdao.checkCredentials(jsonCred);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Invalid user";
	}
	
	//Search for SP
	@POST
	@Path("/search")
	@Produces("application/json")
	public String searchSP(String jsonCat) {		
		IHackAction ihackaction = new IHackAction();
		DB IHackDatabase = ihackaction.initialize();
		IHackDAO ihackdao = new IHackDAO(IHackDatabase);	
		
		try {	
			return ihackdao.getAllSPfromCat(jsonCat);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	//Billing
	@POST
	@Path("/genBill")
	@Produces("application/json")
	public String genBill(String jsonBill) {		
		IHackAction ihackaction = new IHackAction();
		DB IHackDatabase = ihackaction.initialize();
		IHackDAO ihackdao = new IHackDAO(IHackDatabase);	
		
		try {	
			return ihackdao.calcSum(jsonBill);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	//Store offers
	@POST
	@Path("/store")
	@Produces("application/json")
	public String storeOffers(String json) {		
		IHackAction ihackaction = new IHackAction();
		DB IHackDatabase = ihackaction.initialize();
		IHackDAO ihackdao = new IHackDAO(IHackDatabase);	
		
		try {	
			return ihackdao.storeOffers(json);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	//Notifications
	@POST
	@Path("/notifs")
	@Produces("application/json")
	public String notify(String json) {		
		IHackAction ihackaction = new IHackAction();
		DB IHackDatabase = ihackaction.initialize();
		IHackDAO ihackdao = new IHackDAO(IHackDatabase);	
		
		try {	
			return ihackdao.getAllRelevantOffers(json);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

}
