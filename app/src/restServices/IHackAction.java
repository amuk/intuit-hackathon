package com.cisco.ihack;
import java.net.UnknownHostException;
import com.mongodb.DB;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class IHackAction {
	
	public DB initialize() {

		final MongoClient mongoClient;
		final DB IHackDatabase;

		try {
			mongoClient = new MongoClient(new MongoClientURI(
					"mongodb://localhost"));
			IHackDatabase = mongoClient.getDB("ihack");	
			
			System.out.println("Initialized");
			return IHackDatabase;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}