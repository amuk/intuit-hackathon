package com.cisco.ihack;
import java.util.Arrays;
import java.util.List;
import java.util.HashMap;

import org.bson.types.ObjectId;
import org.json.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;


public class IHackDAO {
	DBCollection servProviderDataCollection;
	DBCollection customerCollection;
	DBCollection productsCollection;
	DBCollection loginCollection;
	DBCollection billingCollection;
	DBCollection locationcollection;
	DBCollection offerCollection;
	
	public IHackDAO(final DB IHackDatabase) {
		servProviderDataCollection = IHackDatabase.getCollection("servproviderdata");	
		customerCollection = IHackDatabase.getCollection("customerdata");
		productsCollection = IHackDatabase.getCollection("products");
		loginCollection = IHackDatabase.getCollection("logindetails");
		billingCollection = IHackDatabase.getCollection("bills");
		locationcollection = IHackDatabase.getCollection("currentloc");
		offerCollection = IHackDatabase.getCollection("offers");
	}
	
	
	/*
	 * Add details about a service provider to the database
	 * ====================================================================
	 */
	public String addServProviderDetails(String jsonNewServProvider) {		
		try {
			BasicDBObject document = (BasicDBObject) JSON.parse(jsonNewServProvider);
			ObjectId id = new ObjectId();
			document.put("id", id);
			servProviderDataCollection.insert(document);			
			
			// Insert login details into separate collection
			BasicDBObject doc = (BasicDBObject) JSON.parse(jsonNewServProvider);
			BasicDBObject newdoc = new BasicDBObject();
			
			String user = doc.getString("userName");
			String pass = doc.getString("password");
			
			newdoc.put("id", id);
			newdoc.put("userName", user);
			newdoc.put("password", pass);
			newdoc.put("type", "Business");
			loginCollection.insert(newdoc);							
			
			String spId = id.toString();
			String newspId = "{\"id\" : \"" + spId + "\"}";
			return newspId;
		}catch(Exception e){
			e.printStackTrace();
			return "Error!!";			
		}		
	}
	
	
	/*
	 * Add details about a customer to the database
	 * ====================================================================
	 */
	public String addCustomerDetails(String jsonNewCust) {		
		try {
			BasicDBObject document = (BasicDBObject) JSON.parse(jsonNewCust);
			ObjectId oid = new ObjectId();
			document.put("id", oid);
			customerCollection.insert(document);
			
			// Insert login details into separate collection
			BasicDBObject doc = (BasicDBObject) JSON.parse(jsonNewCust);
			BasicDBObject newdoc = new BasicDBObject();
			
			String user = doc.getString("userName");
			String pass = doc.getString("password");
			
			newdoc.put("id", oid);
			newdoc.put("userName", user);
			newdoc.put("password", pass);
			newdoc.put("type", "Customer");
			loginCollection.insert(newdoc);	
			
			String custId = oid.toString();
			String newcustId = "{\"id\" : \"" + custId + "\"}";
			return newcustId;
		}catch(Exception e){
			e.printStackTrace();
			return "";			
		}		
	}
	
	
	/*
	 * Add new product details to the Rate Card
	 * ====================================================================
	 */
	public String addNewProduct(String jsonNewProduct){
		try {
			BasicDBObject document = (BasicDBObject) JSON.parse(jsonNewProduct);
			ObjectId id = new ObjectId();
			document.put("id", id);
			productsCollection.insert(document);
			String prodId = id.toString();

			String newprodId = "{\"id\" : \"" + prodId + "\"}";
			return newprodId;
		}catch(Exception e){
			e.printStackTrace();
			return "";			
		}		
	}
	
	
	/*
	 * Update current location of SP
	 * ====================================================================
	 */
	public void updateCurrentLoc(String jsonloc){
		try {
			BasicDBObject document = (BasicDBObject) JSON.parse(jsonloc);			
			locationcollection.insert(document);			
		}catch(Exception e){
			e.printStackTrace();					
		}		
	}	
	
	
	/*
	 * Get all customers' information
	 * ====================================================================
	 */
	public String getAllCustomers(){
		JsonObject ret = new JsonObject();
		BasicDBObject whereQuery = new BasicDBObject();
		DBCursor cursor = customerCollection.find(whereQuery);
		JsonArray custArray = new JsonArray();
		JsonParser parser = new JsonParser();
		
		while (cursor.hasNext()) {
			DBObject dock = cursor.next();
			String sdock = dock.toString();			
			JsonObject cust = (JsonObject) parser.parse(sdock);
			custArray.add(cust);
		}
		ret.add("customers", custArray);
		//System.out.println(ret.toString());
		return ret.toString();
	}
	
	
	 
	/*
	 * Calculate distance between two points
	 * ====================================================================
	 */
	public double distance_btwn_two_points(double lat1, double lat2, double lon1, double lon2) {
		final int R = 6371; // Radius of the earth in km

		Double latDistance = Math.toRadians(lat2 - lat1);

		Double lonDistance = Math.toRadians(lon2 - lon1);

		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
		+ Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
		* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		double distance = R * c * 1000; // convert to meters

		return distance;
	}
	
	
	/*
	 * Get all service providers belonging to a particular category
	 * ====================================================================
	 */
	public String getAllSPfromCat(String jsonCategory){
		BasicDBObject document = (BasicDBObject) JSON.parse(jsonCategory);
		
		String cat = (String)document.get("category");
		String lats = (String)document.get("lat"); // Customer's latitude
		String lons = (String)document.get("long"); // Customer's longitude
		String rads = (String) document.get("radius");
		double lat = Double.parseDouble(lats);
		double lon = Double.parseDouble(lons);
		int rad = Integer.parseInt(rads);
		
		JsonObject ret = new JsonObject();
		JsonArray spArray = new JsonArray();
		JsonParser parser = new JsonParser();

		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("category", cat);
		
		DBCursor cursor = servProviderDataCollection.find(whereQuery);
		while (cursor.hasNext()) {
			DBObject dock = cursor.next();
			String id = (String)dock.get("id");
			
			BasicDBObject where = new BasicDBObject();
			where.put("id", id);			
			DBCursor cur = locationcollection.find(where);
			
			if(cur.hasNext()){
				DBObject doc = cursor.next();
				String slat = (String)doc.get("lat"); // SP's latitude
				String slong = (String)doc.get("long"); // SP's longitude
				double splat = Double.parseDouble(slat);
				double splon = Double.parseDouble(slong);
				if(distance_btwn_two_points(splat, lat, splon, lon) <= (rad*1000)){
					String val = dock.toString();
					JsonObject sp = (JsonObject) parser.parse(val);
					spArray.add(sp);
				}
			}
			
			/*String val = dock.toString();
			JsonObject sp = (JsonObject) parser.parse(val);
			spArray.add(sp);*/
		}
		ret.add("servProviders", spArray);

		return ret.toString();
	}	
	
		
	
	/*
	 * Check credentials passed
	 * ====================================================================
	 */
	public String checkCredentials(String credJSON){
		
		BasicDBObject document = (BasicDBObject) JSON.parse(credJSON);
		String user = (String)document.get("userName");
		String pass = (String)document.get("password");
				
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.append("userName", user);
		whereQuery.append("password", pass);
		
		DBCursor cursor = loginCollection.find(whereQuery);		
		
		if(cursor.hasNext())
			return cursor.next().toString();
		else
			return "";
	}
	
	
	/*
	 * Retrieve all products belonging to a particular SP
	 * ====================================================================
	 */
	public String getAllProducts(String jsonSP){
		BasicDBObject document = (BasicDBObject) JSON.parse(jsonSP);
		String id = (String)document.get("id");
		
		JsonObject ret = new JsonObject();
		JsonArray prodArray = new JsonArray();
		JsonParser parser = new JsonParser();
		
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("id", id);
		
		DBCursor cursor = productsCollection.find(whereQuery);
		while (cursor.hasNext()) {
			DBObject dock = cursor.next();
			dock.removeField("id");			
			String val = dock.toString();
			JsonObject prod = (JsonObject) parser.parse(val);
			prodArray.add(prod);
		}
		ret.add("products", prodArray);

		return ret.toString();		
	}

	
	/*
	 * Billing functionality - Calc sum of all values in json
	 * Json depicts all the items bought and their prices
	 * ====================================================================
	 */
	public String calcSum(String jsonBill){
		double total = 0.0;
		BasicDBObject bill = new BasicDBObject();
		
		BasicDBObject document = (BasicDBObject) JSON.parse(jsonBill);
		String id = (String)document.get("id");
		String services = (String)document.get("serviceName");
		List<String> serviceList = Arrays.asList(services.split(","));
		
		for(int i = 0; i < serviceList.size(); i++){
			String service = serviceList.get(i);

			BasicDBObject whereQuery = new BasicDBObject();
			whereQuery.append("serviceName", service);
			whereQuery.append("id", id);
			DBCursor cursor = productsCollection.find(whereQuery);
			
			if(cursor.hasNext()){
				DBObject dock = cursor.next();
				double rate = (double)dock.get("serviceRate");
				total = total + rate;
				bill.put(service, rate);				
			}			
		}		
		bill.put("total", total);
		bill.put("area", (String)document.get("area"));		
		billingCollection.insert(bill);		
		return bill.toString();		
	}
	
	/*
	 * Store offers
	 * ====================================================================
	 */
	public String storeOffers(String jsonoffer){
		try {
			BasicDBObject document = (BasicDBObject) JSON.parse(jsonoffer);
			offerCollection.insert(document);
			return "success";
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error!!";	
		}			
	}
	
	/*
	 * Retrieve all offers
	 * ====================================================================
	 */
	public String getAllOffers(){
		JsonObject ret = new JsonObject();
		BasicDBObject whereQuery = new BasicDBObject();
		DBCursor cursor = offerCollection.find(whereQuery);
		JsonArray offerArray = new JsonArray();
		JsonParser parser = new JsonParser();
		
		while (cursor.hasNext()) {
			DBObject dock = cursor.next();
			String sdock = dock.toString();			
			JsonObject cust = (JsonObject) parser.parse(sdock);
			offerArray.add(cust);
		}
		ret.add("offers", offerArray);
		//System.out.println(ret.toString());
		return ret.toString();		
	}
	
	/*
	 * Retrieve all relevant offers
	 * ====================================================================
	 */
	public String getAllRelevantOffers(String jsonrad){
		BasicDBObject document = (BasicDBObject) JSON.parse(jsonrad);
		String lats = (String)document.get("lat"); // Customer's latitude
		String lons = (String)document.get("long"); // Customer's longitude
		String rads = (String)document.get("radius");
		
		double lat = Double.parseDouble(lats);
		double lon = Double.parseDouble(lons);
		int rad = Integer.parseInt(rads);
		JSONObject ret;
		
		HashMap<String, String> notification_msg = new HashMap<String, String>();;
		
		BasicDBObject whereQuery = new BasicDBObject();
		DBCursor cursor = offerCollection.find(whereQuery);
		
		while(cursor.hasNext()){
			DBObject doc = cursor.next();
			String slat = (String)doc.get("lat"); // SP's latitude
			String slong = (String)doc.get("long"); // SP's longitude
			double splat = Double.parseDouble(slat);
			double splon = Double.parseDouble(slong);			
			if(distance_btwn_two_points(splat, lat, splon, lon) <= (rad*1000)){
				notification_msg.put((String)doc.get("SP"), (String)doc.get("offer"));				
			}			
		}	
		ret =  new JSONObject(notification_msg);
		if(notification_msg.isEmpty())
			return "";
		else
			return ret.toString();
	}
	
}
